# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
Flixter4::Application.config.secret_key_base = 'f0971fec60cd857c4d781ee8e5abc11501bfb0e2319dd8202435c82299d9aa3dfbc2621533a204d1e3f5bf12bef661c0d53edba14f3fa747a2e30ffdc50aed92'
